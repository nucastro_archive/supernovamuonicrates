!!! simple test program to call the 2D/4D subroutines 
program test
   implicit none
   integer,parameter::NP=50 
   real*8:: EnuA(NP),OpaA(NP),OpaA_4D(NP),xTem,cheme,chemn,chemp,&
          masse,massn,massp,xUn,xUp               
   integer:: i
   do i=1,NP
      EnuA(i) = i*5.d0        
   end do      
   xTem  = 20.d0  
   cheme = 40.d0
   chemn = 900.d0
   chemp = 850.d0
   masse = 0.511d0
   massn = 939.57d0 
   massp = 938.27d0 
   xUn = 20.d0
   xUp = -100.d0    
   OpaA = 0.d0
   OpaA_4D = 0.d0   
   call Opacity_CC(0,1,NP,EnuA,OpaA,xTem,cheme,chemn,chemp,masse,massn,massp,xUn,xUp)
   call Opacity_CC_4D(0,1,NP,EnuA,OpaA_4D,xTem,cheme,chemn,chemp,masse,massn,massp,xUn,xUp)       
   write(*,'(50G14.5)') OpaA(1:NP)
   write(*,'(50G14.5)') OpaA_4D(1:NP)          
end program test    
