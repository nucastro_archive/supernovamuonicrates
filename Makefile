#FC=ifort
FC=gfortran
FFLAGS=-O3
LIBS=-lcuba

%.o : %.f90
	$(FC) $(FFLAGS) -c -o $@ $< $(LIBS) 

OBJECTS = Opacity_SemiLep_2D.o Opacity_SemiLep_4D.o test.o

test2D: $(OBJECTS)
	$(FC) $(FFLAGS) -o test $(OBJECTS) $(LIBS) 

clean:
	rm -f $(OBJECTS) *.mod test
