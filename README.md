# SuperNovaMuonicRates
The Fortran subroutines can be used to calculate neutrino (especially muon neutrino) charged-current rate/opacity in relativistic mean field approximation.


**Physics:**
 
*  We consider a full hadronic weak current including weak magnetism, pseudoscalar coupling as well as momentum-dependent form factors.

*  Two different & independent schemes, one involving 2-dimensioanl (2D) integrals and the other with 4-dimensonal (4D) integrals, have been taken to compute the neutrino opacities.  
    Note that form factor effects are only approximately treated in the 2D scheme (opacities are only valid for Enu below 100-150 MeV), while they are fully considered in the 4D scheme.                  
    Without including the form factors, the two schemes can give consistent results within 0.1%.

*   Temperature, relativistic chemical potentials and masses of neutron, proton, and charged leptons, and interaction potentials of neutron and proton  should be provided as inputs.  
    
**To run the codes:**

1.  'Opacity_SemiLep_2D.f90' and 'Opacity_SemiLep_4D.f90' provide the subroutines for our 2D and 4D schemes, respectively.     

2.  The subroutine in 'Opacity_SemiLep_2D.f90' can be directly used.     

3.  For the 4D scheme, we use the Monte Carlo algorithm, Vegas, encoded in the CUBA library to evaluate the 4D integrals. 
    Therefore, the CUBA library should be installed, see http://www.feynarts.de/cuba/. 

    The best option is to use Cuba-4.x. For those whom have installed old versions, please change the inputs of the vegas subroutines called in 'Opacity_SemiLep_4D.f90' (see also the demo program provided by CUBA). For example,
    for those using Cuba-3.x, please remove the parameter 'spin' as an input.  
    
    To run 'Opacity_SemiLep_4D.f90' with gfortran, please use compiler option '-O3' to obtain more reliable results.
    
4.  A simple test program 'test.f90' along with the Makefile are also provided.  

**Some tips for using the codes in supernova simulations**

1.  To use the codes in supernova simulation efficiently, the users may want to reduce the No. of integration grids (parameters 'Ngrids' for 2D, 'nstart' and 'nincrease' for 4D).
2.  The 2D subroutine runs more efficiently than the 4D subroutine. However, when form factor effects are considered, it results in inaccurate opacities for Enu above ~150MeV.
 
    **In this case, we suggest using the 2D subroutine to compute opacity for Enu below 100 MeV, and use the 4D subroutine for a few energy grids above 100 MeV.**

    **Alternatively, the user can tabulate the opacities offline & then do interpolation over the tables in supernova simulations.**  
      


**Please contact Gang Guo (gangg23@gmail.com) for any questions.**
